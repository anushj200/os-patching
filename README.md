**OS Patching playbook:**



> OS patching playbok will update OS patches on RHEL and Debian and Windows flavours


**os_patch project directory structure:**

```
os-patching:
--> roles
    --> os_patch
        ---> tasks
```


**Steps needs to be perform:**

```
1. prepare inventory file
2. Run the playbook
```

for Linux:
`ansible-playbook -i inv osupdate.yaml`

for Windows:
`ansible-playbook -i inv2 osupdate.yaml`


